package Base;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TestBase {
	
	public static Response response;
	public static Results rs = new Results();
	
	public static Response sendRequest(String baseURI, Object obj) throws ParseException
	{
		String sendJson = new Gson().toJson(obj);
		RestAssured.baseURI = baseURI;
		RequestSpecification request = RestAssured.given()
				.header("content-type", "application/json")
				.body(sendJson);
		Response response = request.post();
		System.out.println("Response body: " + response.body().asString());
		
		
		return response;
	}
	
	public static String getValueOfNestedKey(String location, String key) throws ParseException {
		Object obj = new JSONParser().parse(response.asString());
		JSONObject jsonObj = new JSONObject((Map) obj);
		JSONObject locationObj = (JSONObject) jsonObj.get(location);
		String value = locationObj.get(key).toString();
		return value;
		
	}
	
	public static String getValueofObjectKey(String key) throws ParseException
	{
		Object obj = new JSONParser().parse(response.asString());
		 JSONObject jsonObj = new JSONObject((Map) obj);
		 String value =  jsonObj.get(key).toString();
		return value;
		
	}
	public static JsonNode parse(String src) throws IOException {
		ObjectMapper objectMapper= new ObjectMapper();
		return objectMapper.readTree(src);
	}
	
	public static String createJson(Results rs) throws IOException
	{
		String appPath = rs.path;
		FileWriter jsonFile = new FileWriter(appPath);
		String json = new Gson().toJson(rs); 
		jsonFile.write(json.toString());
		jsonFile.close();
		return json;
	}
}
