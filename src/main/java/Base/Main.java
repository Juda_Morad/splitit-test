package Base;

import java.io.IOException;

import org.json.simple.parser.ParseException;

public class Main extends TestBase {

	final static String LOGIN_BASE_URI = "https://webapi.sandbox.splitit.com/api/Login?format=json";
	final static String INIT_BASE_URI = "https://webapi.sandbox.splitit.com/api/InstallmentPlan/Initiate?format=json";
	final static String CREATE_BASE_URI = "https://webapi.sandbox.splitit.com/api/InstallmentPlan/Create?format=json"; 
	final static int RUN_NUM = 20;
	

	public static SpltitResponse newTransaction(CreditCardDetails ccd) throws ParseException {
		
		SpltitResponse spRes = new SpltitResponse();
		Login login = new Login();
		response = sendRequest(LOGIN_BASE_URI ,login);
		spRes.sessionId = getValueofObjectKey("SessionId");
		spRes.isLoginSuccedded = getValueOfNestedKey("ResponseHeader", "Succeeded");

		NewPayment newPayment = new NewPayment(spRes.sessionId);
		PlanData planData = new PlanData();
		newPayment.planData = planData;
		response = sendRequest(INIT_BASE_URI, newPayment);

		spRes.InstallmentPlanNumber = getValueOfNestedKey("InstallmentPlan", "InstallmentPlanNumber");
		newPayment.setInstallmentPlanNumber(spRes.InstallmentPlanNumber);
		response = sendRequest(INIT_BASE_URI, newPayment);

		
		BillingAddress billingAddress = new BillingAddress();
		ConsumerData consumerData = new ConsumerData();

		newPayment.creditCardDetails = ccd;
		newPayment.billingAddress = billingAddress;
		newPayment.consumerData = consumerData;
		response = sendRequest(INIT_BASE_URI, newPayment);

		PlanApprovalEvidence approvalEvidence = new PlanApprovalEvidence();
		newPayment.planApprovalEvidence = approvalEvidence;
		response = sendRequest(CREATE_BASE_URI, newPayment);
		spRes.isTransactionPass = getValueOfNestedKey("ResponseHeader", "Succeeded");
		spRes.errors = getValueOfNestedKey("ResponseHeader","Errors");
		rs.runResults.add(spRes);
		return spRes;
	}
	
	public static void main(String[] args) throws ParseException, IOException {
		
		for (int i = 0; i<= RUN_NUM; i++)
		{
			CreditCardDetails creditCardDetails = new CreditCardDetails();
			SpltitResponse sp = newTransaction(creditCardDetails);
			
		}
		
		for (int g = 0; g <= RUN_NUM; g++)
		{
			CreditCardDetails creditCardDetails = new CreditCardDetails();
			creditCardDetails.setCardNumber("4222222222222220");
			creditCardDetails.setCardExpYear("20");
			creditCardDetails.setCardExpMonth("10");
			newTransaction(creditCardDetails);
		}
		
		createJson(rs);
		
	}

}
