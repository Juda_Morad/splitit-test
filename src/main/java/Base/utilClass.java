package Base;

import java.util.ArrayList;
import java.util.Random;

import com.github.javafaker.Faker;

public class utilClass extends TestBase{
}


class Login{
	private String userName = "test_vpos";
	private String password = "Test123!";

}
class RequestHeader{

	final String ApiKey = "482d437f-06e8-46e9-bcaf-15d7b776ec4d";
	private String sessionId = null;

	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}
class Amount {

	final String currencyCode = "GBP";
	private int value = 0;
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}


}

class PlanData{
	public PlanData(){
		Random rand = new Random();
		amount = new Amount();
		amount.setValue(rand.nextInt(5000));
	}
	public  Amount amount;
	private String refOrderNumber = "123456879";
	private String PurchaseMethod = "inStore";
	private int numberOfInstallments = 6;
}

class NewPayment{
	
	public RequestHeader requestHeader;
	public PlanData planData;
	private String installmentPlanNumber = null;
	public CreditCardDetails creditCardDetails;
	public BillingAddress billingAddress;
	public ConsumerData consumerData;
	public PlanApprovalEvidence planApprovalEvidence;
	
	public String getInstallmentPlanNumber() {
		return installmentPlanNumber;
	}

	public void setInstallmentPlanNumber(String installmentPlanNumber) {
		this.installmentPlanNumber = installmentPlanNumber;
	}

	public NewPayment(String sessionId) {
		requestHeader = new RequestHeader();
		requestHeader.setSessionId(sessionId);
	}
}
class CreditCardDetails{
	private int cardCvv = 123;
	private String cardHolderName = "Juda";
	private String cardNumber = "4111111111111111";
	private String cardExpYear = "22";
	private  String cardExpMonth = "02";

	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCardExpYear() {
		return cardExpYear;
	}
	public void setCardExpYear(String cardExpYear) {
		this.cardExpYear = cardExpYear;
	}
	public String getCardExpMonth() {
		return cardExpMonth;
	}
	public void setCardExpMonth(String cardExpMonth) {
		this.cardExpMonth = cardExpMonth;
	}
}

class ConsumerData {
	public String fullName = null;
	public String cultureName = "en-US";
	public String email = null;
	public String phoneNumber = "999999999999";
	
	public ConsumerData() {
		Faker faker = new Faker();
		fullName = faker.name().firstName();
		email = fullName+"@gmail.com";
	}
}
class BillingAddress{
	String addressLine = "Ben Gurion";
	String addressLine2 = "33";
	String city = "Ramat gan";
	String state ="merkaz";
	String country = "ISRAEL";
	String zip = "123456";
}

class PlanApprovalEvidence {
	public String customerSignaturePngAsBase64 = "+vggQIECAwLDAa2I3vF1XI0CA";
}
class SpltitResponse{
	public String sessionId = null;
	public String InstallmentPlanNumber = null;
	public String isLoginSuccedded = null;
	public String isTransactionPass = null;
	public String errors = null;
}
class Results {
	ArrayList<SpltitResponse> runResults;
	String path = System.getProperty("user.dir")+"//Results//results.json"; 
	public Results() {
		runResults = new ArrayList<SpltitResponse>();
	}
}
